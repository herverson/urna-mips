.data
	# mensagens para pedir as informações
	deputadoFederal: .asciiz "\nDeputado federal\n"
	governador: .asciiz "\nGovernador\n "
	presidente: .asciiz "\nPresidente\n "
	apuracao: .asciiz "\nApuração\n"
	traco: .asciiz "\n---------------------------------\n"
	
	# candidatos presidentes
	presidenteMaria: .asciiz "54321"
	presidenteJoao: .asciiz "12345"
	presidenteMoises: .asciiz "22222"
	
	# candidato depudato federal
	deputadoFederalFrancisca: .asciiz "98796"
	deputadoFederalAntonio: .asciiz "89898"
	deputadoFederalMoises: .asciiz "10107"
	
	
	# candidato governador
	GovernadorJulia: .asciiz "44444"
	GovernadorCarlos: .asciiz "55555"
	GovernadorMoises: .asciiz "12227"
	
	# resultados
	
	presiMaria: .asciiz "\nVotos Presidente maria "
	presiJoao: .asciiz "\nVotos presidente joão "
	presiMoises: .asciiz "\nVotos presidente Moises "
	
	federalFrancisca: .asciiz "\nVotos deputado federal Francisca "
	federalAntonio: .asciiz "\nVotos deputado federal Antonio "
	federalMoises: .asciiz "\nVotos deputado federal Moises "
	
	govJulia: .asciiz "\nVotos governador Julia "
	govCarlos: .asciiz "\nVotos governador Carlos "
	govMoises: .asciiz "\nVotos governador Moises "
	
	nulos: .asciiz "\nVotos nulos "
	total: .asciiz "\nTotal de votos "
	
	sair: .asciiz "00000"	
	# mensagens de resultado
	voto: .space 6
	
.text
.globl main
main:
	j presidentefunction
presidentefunction:
	# presidente
	li $v0,4
	la $a0,presidente
	syscall
	li $v0,8
	la $a0,voto
	addi $a1,$zero,6
	syscall   
	# caso 00000 sair
	la $a0,sair  #passa a string para o endereço
	la $a1,voto  #passa a string para o endereço
	jal strcmp  #chama strcmp

	beq $v0,$zero,sairVotacao #checa o resultado
	addi $t6,$t6,1	
	la $a0,presidenteMaria  #passa a string para o endereço
	la $a1,voto  #passa a string para o endereço
	jal strcmp  #chama strcmp

	beq $v0,$zero,okpresiMaria #checa resultado

	la $a0,presidenteJoao  #passa a string para o endereço
	la $a1,voto  #passa a string para o endereço
	jal strcmp  #chama strcmp
 
	beq $v0,$zero,okpresiJoao #checa o resultado
	
	la $a0,presidenteMoises  #passa a string para o endereço
	la $a1,voto  #passa a string para o endereço
	jal strcmp  #chama strcmp
 
	beq $v0,$zero,okpresiMoises #checa o resultado
	# nulo
	addi $k1,$k1,1
	j deputadoFederalfunction


deputadoFederalfunction:
	# deputado federal
	li $v0,4
	la $a0,deputadoFederal
	syscall
	li $v0,8
	la $a0,voto
	addi $a1,$zero,6
	syscall   
	# caso 00000 sair
	la $a0,sair  #passa a string para o endereço
	la $a1,voto  #passa a string para o endereço
	jal strcmp  #chama strcmp

	beq $v0,$zero,sairVotacao #checa o resultado
	
	la $a0,deputadoFederalFrancisca  #passa o endereço para str1
	la $a1,voto  #passa o endereço para str2
	jal strcmp  #chama strcmp

	beq $v0,$zero,okdepfedFrancisca #checa resultado

	la $a0,deputadoFederalAntonio  #passa o endereço para str1
	la $a1,voto  #passa o endereço para str2
	jal strcmp  #chama strcmp

	beq $v0,$zero,okdepfedAntonio #verifica resultado
	
	la $a0,deputadoFederalMoises  #passa o endereço para str1
	la $a1,voto  #passa o endereço para str2
	jal strcmp  #chama strcmp

	beq $v0,$zero,okdepfedMoises #verifica resultado
	
	# nulo
	addi $k1,$k1,1
	j governadorFunction

# governador
governadorFunction:
	# governador
	li $v0,4
	la $a0,governador
	syscall
	li $v0,8
	la $a0,voto
	addi $a1,$zero,6
	syscall   #string 2
	# caso 00000 sair
	la $a0,sair  #pass address of str1
	la $a1,voto  #pass address of str2
	jal strcmp  #call strcmp

	beq $v0,$zero,sairVotacao #checa resultado
	
	la $a0,GovernadorJulia  #passa o endereço para str1
	la $a1,voto  #passa o endereço para str2
	jal strcmp  #chama strcmp

	beq $v0,$zero,okgovJulia #checa resultado

	la $a0,GovernadorCarlos  #passa o endereço para str1
	la $a1,voto  #passa o endereço para str2
	jal strcmp  #chama strcmp

	beq $v0,$zero,okgovCarlos #verifica resultado
	
	la $a0,GovernadorMoises  #passa o endereço para str1
	la $a1,voto  #passa o endereço para str2
	jal strcmp  #chama strcmp

	beq $v0,$zero,okgovMoises #verifica resultado
	
	# nulo
	addi $k1,$k1,1
	j presidentefunction


# votos deputados federais	
okdepfedFrancisca:
	addi $s3,$s3,1

	j governadorFunction
okdepfedAntonio:
	addi $s4,$s4,1

	j governadorFunction

okdepfedMoises:
	addi $s5, $s5, 1

	j governadorFunction
	
# votos governador
okgovJulia:
	addi $s0,$s0,1

	j presidentefunction
okgovCarlos:
	addi $t7,$t7,1

	j presidentefunction
okgovMoises:
	addi $s6,$s6,1

	j presidentefunction	
			
okpresiJoao:
	addi $s2,$s2,1

	j deputadoFederalfunction
okpresiMaria:
	addi $s1,$s1,1

	j deputadoFederalfunction
okpresiMoises:
	addi $s7,$s7,1

	j deputadoFederalfunction
exit:
	li $v0,10
	syscall
sairVotacao:
	li $v0,4
	la $a0,apuracao
	syscall
	li $v0,4
	la $a0,traco
	syscall
	
	# votos presidente
	li $v0,4
	la $a0,presiMaria
	syscall
	li $v0,1
	move $a0,$s1
	syscall           #print int	
	
	
	li $v0,4
	la $a0,presiJoao
	syscall
	li $v0,1
	move $a0,$s2
	syscall           #print int
	
	li $v0,4
	la $a0,presiMoises
	syscall
	li $v0,1
	move $a0,$s7
	syscall           #print int	
	
	
	# votos federal
	li $v0,4
	la $a0,federalFrancisca
	syscall
	li $v0,1
	move $a0,$s3
	syscall           #print int	
	
	
	li $v0,4
	la $a0,federalAntonio
	syscall
	li $v0,1
	move $a0,$s4
	syscall           #print int
	
	li $v0,4
	la $a0,federalMoises
	syscall
	li $v0,1
	move $a0,$s5
	syscall          
	

	# votos governador
	li $v0,4
	la $a0,govJulia
	syscall
	li $v0,1
	move $a0,$s0
	syscall           #print int	
	
	
	li $v0,4
	la $a0,govCarlos
	syscall
	li $v0,1
	move $a0,$t7
	syscall           #print int
	
	
	li $v0,4
	la $a0,govMoises
	syscall
	li $v0,1
	move $a0,$s6
	syscall     
	     
	# total votos
	li $v0,4
	la $a0,total
	syscall
	li $v0,1
	move $a0,$t6
	syscall           #print int
	# votos nulos
	li $v0,4
	la $a0,nulos
	syscall
	li $v0,1
	move $a0,$k1
	syscall           #print int	
	li $v0,10
syscall

strcmp:
	add $t0,$zero,$zero
	add $t1,$zero,$a0
	add $t2,$zero,$a1
loop:
	lb $t3($t1)  #carrega um byte para cada char
	lb $t4($t2)
	beqz $t3,checa #fim string 1
	beqz $t4,resultado
	sne $t5,$t3,$t4  #compara dois bytes
	bnez $t5,resultado
	addi $t1,$t1,1  #t1 para o proximo endereço da string
	addi $t2,$t2,1
	j loop

resultado: 
	addi $v0,$zero,1
	j fimfuncao
checa:
	bnez $t4,resultado
	add $v0,$zero,$zero

fimfuncao:
	jr $ra